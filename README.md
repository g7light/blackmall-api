# Documentação da API Blackmall para consumo dos produtos registrados.

- - -

Estrutura de um modelo de produto básico para ser retornado em formato JSON. Quaisquer informações com alguma particularidade que não se adeque aos campos disponíveis no modelo, deve ser preenchida no campo **extraInfo**

Este modelo de objeto é o básico para que seja possível o cadastro e integração na base de dados, portanto é possível que conforme o desenvolvimento da integração, o mesmo tenha novos campos.

**Produto:**

* id: Identificador do produto formatado sem caracteres especiais e espaços
* name: Nome do produto

* description: Descrição do produto

* price: Preço do produto sem formatação com pontos

* productId: Identificador do produto em siglas 

* category: Nome da categoria do produto, o retorno deverá sem String, pois esta categoria será cadastrada e recebera o ID (Number) para associação no modelo **Produto**

+ store: Loja em que o produto será relacionado
    * name: Nome da Loja  
+ images: 
    * gallery: Array de imagens contendo a URL da mesma
    * thumb: URL da imagem em thumbnail
* extraInfo: Qualquer informação com certa particularidade que não se adeque nos campos solicitados, deve ser preenchida neste campo

- - -

**Exemplo de estrutura montada em Javascript:**



    id:          { type: String, index: {unique: true}, set: utils.string.urlFormatter }
    name:        { type: String, required: true }
    description: { type: String, required: true }
    price:       { type: Number, required: true }
    productId:   { type: String }
    category:    { type: String }
    store:
      name: { type: String }
    images:
      gallery: { type: Array }
      thumb:   { type: String }
    extraInfo: { type: String }

- - - 

**Exemplo de produto que deverá ser retornado:**

    {
	"_id" : ObjectId("53188dd0ee72bef07c413ba2"),
	"category" : "categoriaAssociada",
	"description" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque facilisis vestibulum ultrices.",
	"id" : "eau-de-parfum",
	"images" : {
		"gallery" : [
			"/images/stores/burberry/products/eau-de-parfum_gallery_0.jpg",
			"/images/stores/burberry/products/eau-de-parfum_gallery_0.jpg",
			"/images/stores/burberry/products/eau-de-parfum_gallery_0.jpg",
			"/images/stores/burberry/products/eau-de-parfum_gallery_0.jpg"
		],
		"thumb" : "/images/stores/burberry/products/eau-de-parfum_thumb.jpg"
	    },
	"name" : "Eau de Parfum",
	"price" : 5999,
	"productId" : "PF03",
	"store" : {
		"name" : "Burberry",
		"id" : "burberry"
	    },
        "extraInfo" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque facilisis vestibulum ultrices Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque facilisis vestibulum ultrices"
    }